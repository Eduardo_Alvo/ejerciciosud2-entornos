package principal;

public class Principal {

	public static void main(String[] args) {
		String cadena1 = "Qwerty";
		String cadena2 = "Uiopq";
		cadena1.toLowerCase();
		cadena2.toUpperCase();
		
		if (cadena1.compareToIgnoreCase(cadena2) < 0) {
			System.out.println(cadena1.charAt(0));
		}
		else {
			System.out.println(cadena2.charAt(0));
		}

	}

}
